import biplist
import datetime
import subprocess
import tempfile
import curses
import os
import sys
import passplist

BT_PLIST = os.path.expanduser('~/Library/Preferences/com.surteesstudios.Bartender.plist')
BT_TRIAL_KEY = 'trialStart2'


def read_trial_key():
    """
    :rtype: datetime.datetime
    """
    pl1 = biplist.readPlist(BT_PLIST)
    return pl1[BT_TRIAL_KEY]


def set_trial_key_to_now():
    tk = read_trial_key()
    old = biplist.readPlist(BT_PLIST)
    old[BT_TRIAL_KEY] = datetime.datetime.now()

    biplist.writePlist(old, BT_PLIST)
    print tk


class FSUsageParser(object):
    def __init__(self):
        os.environ["TERM"] = "xterm-256color"
        self.screen = curses.initscr()
        self.ovf = 0
        self.ovf_count = 0

    def write_str(self, s1, *attrs):
        max_y, max_x = self.screen.getmaxyx()
        cur_y, cur_x = self.screen.getyx()
        if max_y > 20:
            if self.ovf_count > 5:
                sys.exit()
            max_y = 4
            max_x = 80
            self.screen.clear()
            self.screen.move(0, 0)
            self.screen.resize(10, 80)
            self.screen.refresh()
            sys.stdout.write("\b" * (self.ovf - 90))
            self.ovf = 0
            self.ovf_count += 1
        else:
            self.ovf += len(s1)
            self.screen.resize(max(max_y, cur_y + 4), max(max_x, len(s1) + 10))

            self.screen.move(cur_y + 1, 0)
            self.screen.refresh()
            sys.stdout.write(s1)
            #self.screen.addstr(s1, *attrs)
            self.screen.refresh()

    def run(self):
        stdof = tempfile.NamedTemporaryFile()
        stdo = stdof.file
        curses.flash()
        p1 = subprocess.Popen("sudo -S fs_usage -w -t 6", stdout=stdo, stdin=subprocess.PIPE, shell=True)
        passpl1 = passplist.load_from_standard_path()
        assert passpl1.get_as_demangled("sudo_password") is not None, \
            "You need to run passpl1.add_as_mangled(\"sudo_password\", \"YOUR_SUDO_PASSWORD\")"
        p1.communicate(str(passpl1.get_as_demangled("sudo_password")) + "\n")

        dim = self.screen.getyx()
        #self.screen.attron(curses.A_UNDERLINE)
        self.screen.refresh()
        p1.wait()
        print stdo.tell()
        stdo.seek(0)
        for line in stdo:
            self.write_str(line, curses.A_UNDERLINE)

        #self.screen.attron(curses.A_UNDERLINE)
        print "hi"
        self.screen.standend()
        self.screen.addstr("hello", curses.A_STANDOUT)
        self.screen.addstr("hello", curses.A_NORMAL)
        self.screen.redrawwin()
        self.screen.refresh()

fsup = FSUsageParser()
fsup.run()
